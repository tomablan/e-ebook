const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');


let config = {
  'entry' : ['./assets/js/main.js', './assets/scss/main.scss'],
  'output' : {
    'filename' : './public/app.js'
  },
  'module' : {
    rules: [
      {
        test : '/\.js$/',
        exclude : '/node_modules/',
        loader : 'babel-loader'
      },
      { // sass / scss loader for webpack
        test: /\.(sass|scss|css)$/,
        loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
      }
    ]
  },
  'plugins': [
    new ExtractTextPlugin({
      filename: './public/app.css',
      allChunks: true,
    }),
  ],

}

module.exports = config;
